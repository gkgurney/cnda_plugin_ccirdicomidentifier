package org.nrg.xnat.ccir.dicom;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.MatchedPatternExtractor;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unused")
public class CCIRNumberExtractor extends MatchedPatternExtractor {
    public CCIRNumberExtractor(final int tag, final String regEx, final int group) {
        super(tag, Pattern.compile(regEx), group);
    }

    public String extract(final DicomObject dicomObject) {
        final String value = super.extract(dicomObject);
        return value == null ? null : value.replace("-", "_");
    }
}
