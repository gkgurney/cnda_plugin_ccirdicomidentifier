package org.nrg.xnat.ccir.dicom;

import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.MatchedPatternExtractor;
import org.nrg.dcm.TextExtractor;
import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("unused")
@Slf4j
public final class CCIRAttributeExtractor extends TextExtractor {
    public CCIRAttributeExtractor(final int tag, final MatchedPatternExtractor ccirNumberExtractor) {
        super(tag);
        _ccirNumberExtractor = ccirNumberExtractor;
        _otherTextExtractor = null;
    }

    public CCIRAttributeExtractor(final int tag, final CCIRAttributeExtractor otherTextExtractor) {
        super(tag);
        _otherTextExtractor = otherTextExtractor;
        _ccirNumberExtractor = null;
    }

    public CCIRAttributeExtractor(final int tag) {
        super(tag);
        _otherTextExtractor = null;
        _ccirNumberExtractor = null;
    }

    @Override
    public String extract(final DicomObject dicomObject) {
        // If the tag is empty, return null. 
        final String content = dicomObject.getString(getTag());
        if (StringUtils.isNotBlank(content)) {
            if (_ccirNumberExtractor != null) {
                final String value = _ccirNumberExtractor.extract(dicomObject);
                return StringUtils.isNotBlank(value) ? value + '_' + content : content;
            } else if (_otherTextExtractor != null) {
                final String value = _otherTextExtractor.extract(dicomObject);
                return StringUtils.isNotBlank(value) ? value + '_' + content : content;
            } else {
                return content;
            }
        } else if (_ccirNumberExtractor != null) {
            final String value = _ccirNumberExtractor.extract(dicomObject);
            return StringUtils.defaultIfBlank(value, "unknown");
        } else {
            return "unknown";
        }
    }

    private int getTag() {
        return getTags().first();
    }

    private final MatchedPatternExtractor _ccirNumberExtractor;
    private final CCIRAttributeExtractor  _otherTextExtractor;
}
