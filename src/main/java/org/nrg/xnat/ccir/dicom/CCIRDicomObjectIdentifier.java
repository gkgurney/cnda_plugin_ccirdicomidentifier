package org.nrg.xnat.ccir.dicom;

import org.nrg.dcm.ChainExtractor;
import org.nrg.dcm.id.ClassicDicomObjectIdentifier;
import org.nrg.dcm.id.CompositeDicomObjectIdentifier;
import org.nrg.xft.security.UserI;
import org.springframework.stereotype.Service;
import javax.inject.Provider;

import lombok.extern.slf4j.Slf4j;

@Service
public final class CCIRDicomObjectIdentifier extends CompositeDicomObjectIdentifier {
    public CCIRDicomObjectIdentifier(final Provider<UserI> userProvider, final CCIRProjectIdentifier project, final CCIRAttributeExtractor subject, final CCIRAttributeExtractor session) {
        super(project, subject, session, new ChainExtractor(ClassicDicomObjectIdentifier.getAAExtractors()));
        setUserProvider(userProvider);
    }
}
