package org.nrg.xnat.ccir.dicom;

import com.google.common.collect.ImmutableSortedSet;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.nrg.config.services.ConfigService;
import org.nrg.dcm.id.DicomProjectIdentifier;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.security.UserI;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;

import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@Accessors(prefix = "_")
public final class CCIRProjectIdentifier implements DicomProjectIdentifier {
    /**
     * Class constructor - Sets the _aeTitle for this class.
     *
     * @param tag - The DICOM tag this ProjectIdentifier is associated with.
     */
    public CCIRProjectIdentifier(final ConfigService configService, final int tag) {
        _configService = configService;
        _tag = tag;
    }

    @Override
    public XnatProjectdata apply(final UserI user, final DicomObject dicomObject) {
        final String projectId = getProjectId(dicomObject);
        if (StringUtils.isBlank(projectId)) {
            return null;
        }
        return XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
    }

    private String getProjectId(final DicomObject dicomObject) {
        final String projectId = dicomObject.getString(getTag());
        if (StringUtils.isBlank(projectId)) {
            return null;
        }
        final Map<String, String> projectMap = getProjectMap();
        if (projectMap == null) {
            return null;
        }
        if (projectMap.containsKey(projectId)) {
            return projectMap.get(projectId);
        }
        return projectId;
    }

    private Map<String, String> getProjectMap() {
        final String configuration = getConfigService().getConfigContents(CCIR_TOOL, CCIR_CONFIG_PATH);
        if (StringUtils.isBlank(configuration)) {
            log.error("Couldn't retrieve CCIR configuration file");
            return null;
        }

        final Map<String, String> projectMap = new HashMap<>();
        for (final String line : configuration.split("\n")) {
            final String[] keyValue = line.split("=");
            if (StringUtils.isNoneBlank(keyValue[0], keyValue[1])) {
                projectMap.put(keyValue[0], keyValue[1]);
            }
        }
        if (projectMap.isEmpty()) {
            log.error("Couldn't retrieve project map from CCIR configuration: \"{}\"", configuration);
            return null;
        }
        return projectMap;
    }

    @Override
    public void reset() {
        // Nothing to do here since this is just set at initialization.
    }

    private static final String CCIR_TOOL        = "CCIR";
    private static final String CCIR_CONFIG_PATH = "projectMapConfig";

    private final SortedSet<Integer> _tags = ImmutableSortedSet.of();

    private final ConfigService _configService;
    private final int           _tag;
}
