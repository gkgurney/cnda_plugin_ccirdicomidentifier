package org.nrg.xnat.ccir.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ComponentScan;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@XnatPlugin(value = "cnda_plugin_ccirdicomidentifier",
            name = "XNAT 1.7 CCIR DICOM Identifier Plugin",
            description = "This is the XNAT 1.7 CCIR DICOM Identifier Plugin.",
            entityPackages = "org.nrg.framework.orm.hibernate.HibernateEntityPackageList",
            logConfigurationFile = "META-INF/resources/cnda_plugin_ccirdicomidentifier-logback.xml")
@ComponentScan("org.nrg.xnat.ccir.plugin")
public class CcirDicomIdentifierPlugin {
}
